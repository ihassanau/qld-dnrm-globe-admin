app.service('dataService', ['$http', '$q',
    function ($http, $q) {


        this.getAllVersions = function (file) {

            var deferred = $q.defer();
            $http.post('/api/configeditor/configs/list', {}).
                then(function (data, status) {

                    return deferred.resolve(data);

                }, function (data, status) {
                    return deferred.reject(false);
                });

            return deferred.promise;
        };

        this.getDefaultTOCContent = function () {
            var deferred = $q.defer();
            $http.post('/api/configeditor/configs/newtoc', {}).
                then(function (data, status) {

                    return deferred.resolve(data);

                }, function (data, status) {
                    return deferred.reject(false);
                });

            return deferred.promise;

        };

        this.createDownloadLink = function (fileName, fileContent) {
            var deferred = $q.defer();
            $http.post('/api/configeditor/configs/download-config-item', { yamlContent: fileContent, yamlfileName: fileName }).
                then(function (result, status) {

                    window.open(result.data.Location, '_self');

                }, function (data, status) {
                    return deferred.reject(false);
                });

            return deferred.promise;
        };

        this.addConfigVersion = function (new_configVersion) {
            var deferred = $q.defer();
            $http.post('/api/configeditor/configs/create', { new_configVersion: new_configVersion }).
                then(function (result, status) {

                    return deferred.resolve(result);

                }, function (data, status) {
                    return deferred.reject(false);
                });

            return deferred.promise;
        };

        this.setActiveVersion = function (configVersionItem) {
            var deferred = $q.defer();
            $http.post('/api/configeditor/configs/set-active', { activeConfigItem: configVersionItem }).
                then(function (result, status) {

                    return deferred.resolve(result);

                }, function (data, status) {
                    return deferred.reject(false);
                });

            return deferred.promise;
        };
    }]
);