app.controller("mainPanelCtrl", ['$scope', 'dataService', function ($scope, dataService) {
    $scope.products = ["Milk", "Bread", "Cheese"];
    $scope.srcModal = '/js/map/view/modals/yaml-editor.html';
    $scope.yamlEditorWarningAlert = '#yamlWarningAlter';
    $scope.configEditorContainer = '#divConfigEditor';
    $scope.yamlErrorMessage = '';
    $scope.newTOCItemName = '';
    $scope.rowEditedClass = 'form-group row row-edited';
    $scope.rowDefaultClass = 'form-group row';
    $scope.isSaving=false;
    $scope.rowEditedTRClass = 'tr-edited';



    $scope.loadAllConfigVersions = function () {
        dataService.getAllVersions().then(function (result) {
            $scope.listVersions = [];
            $scope.selectedVersion = null;
            var configVersion = null;
            $scope.maxVersion = 0;
            var configHighestVersion;
            angular.forEach(result.data.models, function (value, key) {
                if (value.message) {
                    configVersion = {
                        name: value.message,
                        id: value._id,
                        configItems: value.configItems,
                        tocItems: $scope.parseTOCItems(value.configItems.tocItems),
                        version: value.version,
                        applicationVersion: value.applicationVersion,
                        active: value.enabled,
                        db_doc: value,
                        configItemsClasses: {},
                        isEdited: false,
                        isSaved: true
                    };


                    configVersion.configItemsClasses['settings'] = $scope.rowDefaultClass;
                    configVersion.configItemsClasses['layerServices'] = $scope.rowDefaultClass;

                    $scope.listVersions.push(configVersion);
                    if (value.enabled) {
                        configVersion.name += ' (active)';
                    }
                    if ($scope.maxVersion <= configVersion.version) {
                        $scope.maxVersion = configVersion.version;
                        configHighestVersion = configVersion;
                    }
                }
            });
            $scope.selectedVersion = configHighestVersion;

        }, function (error) {
            console.log(error);
        });
    };
    $scope.loadAllConfigVersions();




    dataService.getDefaultTOCContent().then(function (result) {

        $scope.defaultTOCContent = result.data.data;

    }, function (error) {
        console.log(error);
    });

    $scope.parseTOCItems = function (tocYamlItems) {
        //var count = tocYamlItems ? tocYamlItems.length : 0;
        var yamlObject = null, listTOCItems = [];
        angular.forEach(tocYamlItems, function (value, key) {
            try {
                yamlObject = jsyaml.load(value);
                listTOCItems.push({
                    name: yamlObject.name,
                    content: value,
                    styleClass: '',
                    isDeleting: false
                });
            }
            catch (e) {
                console.log('error parsing yaml: ');
                console.log(value);
            }

        });

        return listTOCItems;
    };

    $scope.showYamlEditor = function (configfile, isTOCItem, yamlContent) {
        $('#yamlEditorModal').modal({ show: true, backdrop: 'static' });
        $scope.cmDoc.setValue('');
        $scope.oldYamlContent = '';
        if (yamlContent) {
            $scope.cmDoc.setValue(yamlContent);
        }
        else if ($scope.selectedVersion) {
            if (!isTOCItem) {
                $scope.modalTitle = configfile;
                $scope.cmDoc.setValue($scope.selectedVersion.configItems[configfile]);
                $scope.oldYamlContent = $scope.selectedVersion.configItems[configfile];
            }
            else {
                $scope.modalTitle = $scope.selectedVersion.tocItems[configfile].name;
                $scope.cmDoc.setValue($scope.selectedVersion.tocItems[configfile].content);
                $scope.oldYamlContent = $scope.selectedVersion.tocItems[configfile].content;
            }
            $scope.nowEditingFile = { configfile: configfile, isTOCItem: isTOCItem };

        }
        $scope.cmDoc.options.readOnly = false;
        $scope.cmDoc.setCursor({ line: 0, ch: 0 });
    };

    $scope.viewConfigItem = function (configfile, isTOCItem) {
        $('#yamlEditorModal').modal({ show: true, backdrop: 'static' });
        $scope.cmDoc.setValue('');

        if ($scope.selectedVersion) {
            if (!isTOCItem) {
                $scope.modalTitle = configfile;
                $scope.cmDoc.setValue($scope.selectedVersion.configItems[configfile]);

            }
            else {
                $scope.modalTitle = $scope.selectedVersion.tocItems[configfile].name;
                $scope.cmDoc.setValue($scope.selectedVersion.tocItems[configfile].content);

            }
        }
        $scope.cmDoc.options.readOnly = true;
        $scope.nowEditingFile = null;
    };

    $scope.$on('$includeContentLoaded', function (event) {
        if ($scope === event.currentScope) {
            $scope.initYamlEditor();
        }
    });

    $scope.initYamlEditor = function () {
        $scope.cmDoc = CodeMirror.fromTextArea(document.getElementById("yamlTxtArea"), { lineNumbers: true });
    };
    
    $scope.saveEdits = function () {
        try {

            var yamlEditorContent = $scope.cmDoc.getValue();
            jsyaml.load(yamlEditorContent);

            if ($scope.oldYamlContent != yamlEditorContent && $scope.selectedVersion.isSaved) {
                $scope.createNewConfigVersionObject();
            }

            if (!$scope.selectedVersion.isSaved) {
                if ($scope.nowEditingFile.isTOCItem) {
                    $scope.selectedVersion.tocItems[$scope.nowEditingFile.configfile].styleClass = $scope.rowEditedTRClass;
                    $scope.selectedVersion.tocItems[$scope.nowEditingFile.configfile].content = yamlEditorContent;
                }
                else {
                    $scope.selectedVersion.configItemsClasses[$scope.nowEditingFile.configfile] = $scope.rowEditedClass;
                    $scope.selectedVersion.configItems[$scope.nowEditingFile.configfile] = yamlEditorContent;
                }
            }

            $scope.oldYamlContent = '';
            $scope.closeYamlEditorWarning();
            $('#yamlEditorModal').modal('hide');
        }
        catch (e) {
            console.log('error parsing yaml: ');
            console.log(e);

            $scope.yamlErrorMessage = "Error while parsing YAML content: " + e.message;
            $($scope.yamlEditorWarningAlert).show();
        }
    };

    $scope.uploadFile = function (configfile, isTOCItem) {

        $scope.fileInput = $('<input/>')
            .attr('type', "file")
            .attr('accept', '.yml')
            .css('display', 'none');
        $($scope.configEditorContainer).append($scope.fileInput);
        $scope.fileInput.on('change', function (e) {
            var reader = new FileReader();
            reader.onload = function (loadedFile) {
                var fileContent = loadedFile.target.result;
                $scope.showYamlEditor(null, null, fileContent);
                $scope.nowEditingFile = { configfile: configfile, isTOCItem: isTOCItem };
                $scope.safeApply();
            };

            reader.readAsText(e.target.files[0]);
        });

        $scope.fileInput.trigger("click");

    };

    $scope.addTOC = function () {
        if ($scope.newTOCItemName) {

            var newTOC = {
                name: $scope.newTOCItemName,
                content: $scope.defaultTOCContent
            };
            $scope.selectedVersion.tocItems.push(newTOC);
            $scope.showYamlEditor($scope.selectedVersion.tocItems.length - 1, true);
            $scope.newTOCItemName = '';
        }
    };

    $scope.downloadConfigItem = function (configfile, isTOCItem) {
        if ($scope.selectedVersion) {
            if (isTOCItem) {
                dataService.createDownloadLink($scope.selectedVersion.tocItems[configfile].name + '.yml', $scope.selectedVersion.tocItems[configfile].content);
            }
            else {
                dataService.createDownloadLink(configfile + '.yml', $scope.selectedVersion.configItems[configfile]);
            }
        };
    };

    $scope.setActiveVersion = function () {

        dataService.setActiveVersion($scope.selectedVersion.db_doc).then(function (result) {

            $scope.loadAllConfigVersions();

        }, function (error) {
            console.log(error);
        });
    };

    $scope.addNewConfigVersion = function () {
        var new_configVersion = {};
        new_configVersion.version = $scope.selectedVersion.version;        
        new_configVersion.enabled = false;
        new_configVersion.message = 'version ' + new_configVersion.version;
        new_configVersion.configItems = {};
        new_configVersion.configItems.settings = $scope.selectedVersion.configItems['settings'];
        new_configVersion.configItems.layerServices = $scope.selectedVersion.configItems['layerServices'];
        new_configVersion.configItems.tocItems = $scope.selectedVersion.tocItems.map(function (tocItem) {
            return tocItem.content;
        });

        
        $scope.isSaving=true;
        dataService.addConfigVersion(new_configVersion).then(function (result) {
            
            $scope.isSaving=false;
            $scope.loadAllConfigVersions();

        }, function (error) {
            $scope.isSaving=false;
            console.log(error);
        });
    };

    $scope.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof (fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    $scope.tocStartDelete = function (tocItemIndex) {
        $scope.selectedVersion.tocItems[tocItemIndex].isDeleting=true;
    };

    $scope.tocConfirmDelete = function (tocItemIndex) {
        if($scope.selectedVersion.isSaved){
            $scope.selectedVersion.tocItems[tocItemIndex].isDeleting=false;
            $scope.createNewConfigVersionObject();
        }

        $scope.selectedVersion.tocItems.splice(tocItemIndex, 1);
    };

    $scope.tocCancelDelete = function (tocItemIndex) {
        $scope.selectedVersion.tocItems[tocItemIndex].isDeleting=false;
    };

    $scope.createNewConfigVersionObject = function(){
        var newConfigVersion = {};
        angular.copy($scope.selectedVersion, newConfigVersion);

        $scope.selectedVersion = newConfigVersion;
        $scope.selectedVersion.isEdited = true;
        $scope.selectedVersion.isSaved = false;
        $scope.selectedVersion.version = ($scope.maxVersion + 1);
        $scope.selectedVersion.enabled = false;
        $scope.selectedVersion.message = 'version ' + $scope.selectedVersion.version;
        $scope.selectedVersion.name = $scope.selectedVersion.message + ' (new)';

        $scope.maxVersion = $scope.selectedVersion.version;
        $scope.listVersions.push(newConfigVersion);
    };

    $scope.loadGlobeConfigVersion = function(){
        //var originURL = window.location.protocol+window.location.host;
        var globeURL = '/?configVersion=' + $scope.selectedVersion.version;
        
        window.open(globeURL, '_blank');
    };

    $scope.closeYamlEditorWarning = function(){
        $($scope.yamlEditorWarningAlert).hide();
    };
}]);

