module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['src/app.module.js', 'src/**/*.js'],
        dest: 'dist/<%= pkg.name %>.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'dist/<%= pkg.name %>.min.js': ['dist/<%= pkg.name %>.annotated.js']
        }
      }
    },
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          src: ['dist/*.js'],
          ext: '.annotated.js',
          extDot: 'last'
        }],
      }
    },
    clean: ['dist/'],
    watch: {
      scripts: {
        files: ['src/*.js'],
        tasks: ['clean','concat'],
        options: {
          spawn: false,
        },
      },
    },
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');

  //grunt.loadNpmTasks('grunt-ng-annotate');
  //grunt.loadNpmTasks('grunt-contrib-uglify');


  grunt.registerTask('default', ['clean', 'concat','watch']);
};